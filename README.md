
##### What if Someone Violates my License ?

A CC license terminates automatically when its conditions are violated. For example, if a reuser of CC-licensed material does not provide the attribution required when sharing the work, then the user no longer has the right to continue using the material and may be liable for copyright infringement. The license is terminated for the user who violated the license. However, all other users still have a valid license, so long as they are in compliance.

Under the 4.0 licenses, a licensee automatically gets these rights back if she fixes the violation within 30 days of discovering it.

If you apply a Creative Commons license and a user violates the license conditions, you may opt to contact the person directly to ask them to rectify the situation or consult a lawyer to act on your behalf. Creative Commons is not a law firm and cannot represent you or give you legal advice, but there are lawyers who have identified themselves as interested in representing people in CC-related matters.

# gindex-backend
Backend for gindex for Maintaining User Database

**Support Group can be Found Here - [Here](https://t.me/joinchat/LVLR1U5Gs_9lmHGNGqpxIw)**<br>
Any Issues / Help Regarding Setup, Contact Through the telegram Group

##### without this the Main GIndex wont work, Both should run simultaneously.

#### Deploy to Heroku Directly:
[![Deploy] git chnage

